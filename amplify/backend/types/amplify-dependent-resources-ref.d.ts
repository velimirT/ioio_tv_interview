export type AmplifyDependentResourcesAttributes = {
  api: {
    ioiotvvelimir: {
      GraphQLAPIEndpointOutput: 'string';
      GraphQLAPIIdOutput: 'string';
    };
  };
  auth: {
    ioiotvvelimir23e8cbca: {
      AppClientID: 'string';
      AppClientIDWeb: 'string';
      IdentityPoolId: 'string';
      IdentityPoolName: 'string';
      UserPoolArn: 'string';
      UserPoolId: 'string';
      UserPoolName: 'string';
    };
  };
  storage: {
    IOIOTVVideos: {
      BucketName: 'string';
      Region: 'string';
    };
  };
};
