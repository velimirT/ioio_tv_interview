/**
 *
 * List
 *
 */
import * as React from 'react';
import styled from 'styled-components/macro';
import { useTranslation } from 'react-i18next';
import { messages } from './messages';
import { useEffect, useState } from 'react';
import { ListItem } from '../ListItem';
import { Hub } from 'aws-amplify';

interface Props {
  loadVideos: Function;
  videos: Array<Item>;
  currentVideo: string;
  selectCurrentVideo: Function;
  setOpen: Function;
}

interface Item {
  id: number;
  title: string;
  description: string;
  imageKey: string;
  createdAt: string;
}

export function List(props: Props) {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const { t } = useTranslation();
  const { loadVideos, videos, selectCurrentVideo, currentVideo, setOpen } =
    props;
  const [list, setList] = useState<Item[]>(videos);

  function authListener(data) {
    console.log('Auth', data);
    if (data.payload.event === 'signOut') {
      loadVideos();
      console.log('List cleared 2!!');
    }

    if (data.payload.event === 'signIn') {
      loadVideos();
      console.log('List created 2!!');
    }
  }

  Hub.listen('auth', authListener);

  useEffect(() => {
    loadVideos();
    return () => {
      loadVideos();
      console.log('List cleared 1');
    };
  }, []);
  return (
    <ListEl>
      <h2> {t(...messages.ListTitle())} </h2>
      <Wrapper>
        <ListItem
          item={{
            id: 0,
            title: 'Add Video',
            description: '',
            imageKey: 'upload2.svg',
            createdAt: '',
          }}
          key={0}
          selectCurrentVideo={setOpen}
          currentVideo={'none'}
        />
        {videos.length > 0 ? (
          videos.map((item: Item) => (
            <ListItem
              item={item}
              key={item.id}
              selectCurrentVideo={selectCurrentVideo}
              currentVideo={currentVideo}
            />
          ))
        ) : (
          <li>
            <h3>{t(...messages.noVideos())}</h3>
          </li>
        )}
      </Wrapper>
    </ListEl>
  );
}

const ListEl = styled.div`
  background: rgba(255, 255, 255, 0.7);
  overflow: hidden;
  & h2 {
    text-align: center;
    border-bottom: 1px solid #ccc;
    padding-bottom: 20px;
  }
`;

const Wrapper = styled.ul`
  display: flex;
  flex-wrap: wrap;
  flex-direction: row;
  list-style-type: none;
  padding: 0 20px;
`;
