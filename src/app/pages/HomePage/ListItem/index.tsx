/**
 *
 * List
 *
 */
import * as React from 'react';
import styled from 'styled-components/macro';
import { useState, useEffect } from 'react';
import { Storage } from 'aws-amplify';

interface Props {
  item: {
    id: number;
    title: string;
    description: string;
    imageKey: string;
    createdAt: string;
  };
  selectCurrentVideo: Function;
  currentVideo: string;
}

export function ListItem(props: Props) {
  const { selectCurrentVideo, item, currentVideo } = props;
  const { id, title, description, imageKey, createdAt } = item;
  const createdDate = createdAt ? new Date(createdAt).toDateString() : '';
  const [videoURL, setVideoURL] = useState(
    'https://react.semantic-ui.com/images/wireframe/image.png',
  );
  async function fetchVideo() {
    const videoURL = await Storage.get(imageKey);
    setVideoURL(videoURL);
  }

  useEffect(() => {
    if (imageKey) {
      fetchVideo();
    }
  }, []);

  return (
    <Wrapper
      key={id}
      onClick={() => selectCurrentVideo(videoURL)}
      className={currentVideo === videoURL ? 'active' : ''}
    >
      {imageKey !== 'upload2.svg' ? (
        <Video src={videoURL} />
      ) : (
        <DefaultImage src={imageKey} alt="Upload Video" />
      )}
      <h3>{title}</h3>
      <p>{description}</p>
      <CreatedDate>{createdDate}</CreatedDate>
    </Wrapper>
  );
}

const Wrapper = styled.li`
  position: relative;
  width: calc(25% - 20px);
  padding: 10px;
  display: flex;
  flex-direction: column;
  box-sizing: border-box;
  padding-bottom: 40px;
  align-items: center;
  cursor: pointer;
  border-radius: 10px;
  margin: 0 10px;
  &.active {
    background-color: rgba(0, 0, 0, 0.1);
  }
  & h3,
  p {
    margin: 0;
    margin-top: 10px;
  }
`;

const DefaultImage = styled.img`
  width: 100px;
  margin: 0 auto;
`;

const Video = styled.video`
  width: 100%;
  max-height: 150px;
`;

const CreatedDate = styled.p`
  font-size: 12px;
  font-weight: 600;
  text-align: right;
  position: absolute;
  bottom: 10px;
  right: 10px;
`;
