/**
 *
 * MainVideo
 *
 */
import * as React from 'react';
import styled from 'styled-components/macro';
import { useTranslation } from 'react-i18next';
import { messages } from './messages';

interface Props {
  currentVideo: string;
}

export function MainVideo(props: Props) {
  const { t } = useTranslation();
  const { currentVideo } = props;
  return (
    <Div>
      {currentVideo !== '' ? (
        <Video src={currentVideo} controls />
      ) : (
        <>
          <h2>{t(...messages.noVideosMainSlide())}</h2>
          <p>{t(...messages.noVideosMainSlideSubTitle())}</p>
        </>
      )}
    </Div>
  );
}

const Div = styled.div`
  width: 100%;
  height: 300px;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  padding: 0 20px;
  & p {
    font-size: 20px;
    margin: 0;
    margin-top: 10px;
  }
`;

const Video = styled.video`
  max-width: 70%;
  max-height: 100%;
`;
