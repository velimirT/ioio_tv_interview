/**
 *
 * Modal
 *
 */
import * as React from 'react';
import styled from 'styled-components/macro';
import { useTranslation } from 'react-i18next';
import { messages } from './messages';
import { useState, useRef, useEffect } from 'react';
import { useS3 } from '../Hooks/useS3';

interface Props {
  onClose: Function;
  onSave: Function;
}

interface FileName {
  name: string;
  type: string;
}

export function Modal(props: Props) {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const { t, i18n } = useTranslation();
  const [title, setTitle] = useState<string>('');
  const [description, setDescription] = useState<string>('');
  const [image, setImage] = useState<string>('');
  const { onClose, onSave } = props;
  const [fileName, setFileName] = useState<FileName>();
  const videoFileRef = useRef<HTMLInputElement>(null);
  const [uploadToS3] = useS3();

  function handleVideoChange(e: React.ChangeEvent<HTMLInputElement>) {
    const videoFile = e.target.files ? e.target.files[0] : null;
    if (!videoFile) return;
    const videoFileURL = URL.createObjectURL(videoFile);
    setImage(videoFileURL);
    setFileName(videoFile);
  }

  function saveVideo() {
    const imageKey = uploadToS3(fileName);
    onSave(title, description, imageKey);
  }

  return (
    <Div>
      <Form>
        {t('')}
        <Close onClick={() => onClose()}>
          <img src="close.jpg" alt="close image" />
        </Close>
        <h2>{t(...messages.modalTitle())}</h2>
        <label>{t(...messages.modalTitleField())} </label>
        <input
          type="text"
          placeholder="title"
          onChange={e => setTitle(e.target.value)}
        />
        <label>{t(...messages.modalDescField())}</label>
        <textarea onChange={e => setDescription(e.target.value)}></textarea>
        <div>
          {image && <PriviewVideo src={image} />}
          <input
            type="file"
            css={{ display: 'none' }}
            ref={videoFileRef}
            onChange={handleVideoChange}
          />
          <Upload
            onClick={() => videoFileRef.current && videoFileRef.current.click()}
          >
            {t(...messages.uploadImage())}
            <img src="upload.svg" alt="click to upload your video" />
          </Upload>
        </div>
        <Controls>
          <Accept onClick={saveVideo}>Save</Accept>
          <Reject onClick={() => onClose()}>Cancel</Reject>
        </Controls>
      </Form>
    </Div>
  );
}

const Div = styled.div`
  position: fixed;
  width: 100vw;
  height: 100vh;
  background: rgba(0, 0, 0, 0.5);
  z-index: 10;
  top: 0;
  left: 0;
  display: flex;
  justify-content: center;
`;

const Form = styled.div`
  position: relative;
  width: 400px;
  background: #fff;
  border-radius: 20px;
  padding: 20px;
  overflow: hidden;
  & input,
  textarea,
  label {
    width: 100%;
  }
  & h2 {
    text-align: center;
  }
`;

const Close = styled.div`
  position: absolute;
  top: 5px;
  right: 5px;
  width: 30px;
  height: 30px;
  & img {
    width: 100%;
    height: 100%;
    cursor: pointer;
  }
`;

const PriviewVideo = styled.video`
  margin-top: 10px;
  max-width: 100%;
  max-height: 100px;
`;

const Controls = styled.div`
  position: absolute;
  width: 100%;
  height: 20px;
  bottom: 30px;
  left: 0;
  & button {
    width: 50%;
    padding: 20px;
    cursor: pointer;
  }
`;

const Upload = styled.button`
  color: #fff;
  background-color: #3196bd;
  border: none;
  width: 100%;
  margin: 10px 0;
  padding: 20px;
  cursor: pointer;
  & img {
    width: 20px;
    margin-left: 20px;
  }
`;

const Accept = styled.button`
  color: #fff;
  background-color: green;
  border: none;
`;

const Reject = styled.button`
  color: #fff;
  background-color: red;
  border: none;
`;
