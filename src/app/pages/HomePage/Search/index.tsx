/**
 *
 * Search
 *
 */
import * as React from 'react';
import styled from 'styled-components/macro';
import { useTranslation } from 'react-i18next';
import { messages } from './messages';
import { useEffect, useState } from 'react';

interface Props {
  searchVideos: Function;
}

export function Search(props: Props) {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const { t, i18n } = useTranslation();
  const [searchInput, setSearchInput] = useState<string>('');
  const [ascSort, setAscSort] = useState<boolean>(false);
  const { searchVideos } = props;
  return (
    <Div>
      <Label>{t(...messages.SearchLabel())}</Label>
      <Navigation>
        <Input type="text" onChange={e => setSearchInput(e.target.value)} />
        <Sort onClick={() => setAscSort(!ascSort)}>
          {ascSort ? 'ASC' : 'DESC'}
          <img src={ascSort ? 'sort-up.svg' : 'sort-down.svg'} />
        </Sort>
        <img
          src="search.svg"
          onClick={() => searchVideos({ searchInput, ascSort })}
          alt="search button"
        />
      </Navigation>
    </Div>
  );
}

const Div = styled.div`
  display: flex;
  flex-direction: column;
  padding: 20px;
  background: rgba(255, 255, 255, 0.7);
  align-items: center;
`;

const Label = styled.div`
  display: flex;
  width: 100%;
  justify-content: center;
  font-size: 30px;
  padding-bottom: 20px;
`;

const Input = styled.input`
  width: 80%;
`;

const Navigation = styled.div`
  display: flex;
  flex-direction: row;
  width: 500px;
  & img {
    width: 30px;
    margin-left: 20px;
    cursor: pointer;
  }
  & button {
    margin-left: 20px;
  }
`;

const Sort = styled.button`
  display: flex;
  flex-direction: row;
  align-items: center;
  padding: 5px;
  background: none;
  outline: none;
  border: 1px solid;
  border-radius: 10px;
  cursor: pointer;
  & img {
    width: 20px;
  }
`;
