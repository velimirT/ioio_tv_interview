import * as React from 'react';
import { Helmet } from 'react-helmet-async';
import { Amplify } from 'aws-amplify';
import awsConfig from '../../../aws-exports';
import { AmplifyAuthenticator, AmplifySignOut } from '@aws-amplify/ui-react';
import { Modal } from './Modal';
import { List } from './List';
import { Search } from './Search';
import { MainVideo } from './MainVideo';
import { useHomepageSlice } from './slice';
import { useSelector, useDispatch } from 'react-redux';
import { selectVideos, selectCurrentVideo, modalOpen } from './slice/selectors';
import { PayloadAction } from '@reduxjs/toolkit';

Amplify.configure(awsConfig);

export function HomePage() {
  const dispatch = useDispatch();
  const { actions } = useHomepageSlice();

  const videos = useSelector(selectVideos);
  const currentVideo = useSelector(selectCurrentVideo);
  const modalIsOpen = useSelector(modalOpen);

  async function saveItem(
    title: string,
    description: string,
    imageKey: string,
  ) {
    dispatch(
      actions.uploadVideo({ title, description, imageKey, slug: title }),
    );
  }

  function setOpen(value: boolean) {
    dispatch(actions.setOpen(value));
  }
  return (
    <>
      <Helmet>
        <title>HomePage</title>
        <meta name="description" content="iOiO.tv Interview Velimir" />
        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link rel="preconnect" href="https://fonts.gstatic.com" />
        <link
          href="https://fonts.googleapis.com/css2?family=Poppins:wght@300&display=swap"
          rel="stylesheet"
        ></link>
      </Helmet>
      <AmplifyAuthenticator>
        <MainVideo currentVideo={currentVideo} />
        <Search
          searchVideos={(payload: PayloadAction<any>) =>
            dispatch(actions.searchVideos(payload))
          }
        />
        <List
          videos={videos}
          currentVideo={currentVideo}
          loadVideos={() => dispatch(actions.loadVideos())}
          selectCurrentVideo={(src: string) =>
            dispatch(actions.selectCurrentVideo(src))
          }
          setOpen={() => setOpen(true)}
        />
        {modalIsOpen && (
          <Modal onClose={() => setOpen(false)} onSave={saveItem} />
        )}
        <AmplifySignOut />
      </AmplifyAuthenticator>
    </>
  );
}
