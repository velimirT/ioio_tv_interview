import { PayloadAction } from '@reduxjs/toolkit';
import { createSlice } from 'utils/@reduxjs/toolkit';
import { useInjectReducer, useInjectSaga } from 'utils/redux-injectors';
import { homepageSaga } from './saga';
import { HomepageState } from './types';

export const initialState: HomepageState = {
  videos: [],
  currentVideo: '',
  videosLoading: false,
  videosError: '',
  modalOpen: false,
  uploadVideoLoading: false,
  uploadVideoError: '',
};

const slice = createSlice({
  name: 'homepage',
  initialState,
  reducers: {
    loadVideos(state) {
      state.videosLoading = true;
      state.videosError = '';
    },
    loadVideosSuccess(state, action: PayloadAction<any>) {
      state.videos = action.payload;
      console.log('action.payload', action.payload);
      state.videosLoading = false;
      state.videosError = '';
    },
    loadVideosError(state, action: PayloadAction<any>) {
      state.videosLoading = false;
      state.videosError = action.payload;
    },

    setOpen(state, action: PayloadAction<any>) {
      state.modalOpen = action.payload;
    },

    searchVideos(state, action: PayloadAction<any>) {},
    searchVideosSuccess(state, action: PayloadAction<any>) {
      state.videos = action.payload;
      console.log('action.payload', action.payload);
      state.videosLoading = false;
      state.videosError = '';
    },

    selectCurrentVideo(state, action: PayloadAction<any>) {
      state.currentVideo = action.payload;
    },

    clearVideos(state, action: PayloadAction<any>) {
      state.videos = [];
    },

    uploadVideo(state, action: PayloadAction<any>) {
      state.uploadVideoLoading = true;
      state.uploadVideoError = '';
    },
    uploadVideosSuccess(state, action: PayloadAction<any>) {
      state.videos = state.videos.concat(action.payload);
      console.log('action.payload', action.payload);
      state.uploadVideoLoading = false;
      state.uploadVideoError = '';
      state.modalOpen = false;
    },
    uploadVideosError(state, action: PayloadAction<any>) {
      state.uploadVideoLoading = false;
      state.uploadVideoError = action.payload;
    },
  },
});

export const { actions: homepageActions } = slice;

export const useHomepageSlice = () => {
  useInjectReducer({ key: slice.name, reducer: slice.reducer });
  useInjectSaga({ key: slice.name, saga: homepageSaga });
  return { actions: slice.actions };
};
