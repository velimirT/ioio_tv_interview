import { take, call, put, select, takeLatest } from 'redux-saga/effects';
import { homepageActions as actions } from '.';
import { Amplify, API, graphqlOperation } from 'aws-amplify';
import { listLists, searchLists } from 'graphql/queries';
import { PayloadAction } from '@reduxjs/toolkit';
import { createList } from 'graphql/mutations';
import { Storage } from 'aws-amplify';
// function* doSomething() {}

interface Data {
  listLists: {
    items: Array<Item>;
  };
}

interface Item {
  id: number;
  title: string;
  description: string;
  imageKey: string;
}

interface SearchData {
  searchLists: {
    items: Array<Item>;
  };
}

interface UploadData {
  createList: {
    items: Array<Item>;
  };
}

function* loadVideos() {
  try {
    const { data } = (yield API.graphql(graphqlOperation(listLists))) as {
      data: Data;
      errors: any[];
    };
    if (data.listLists.items.length < 1) {
      yield put(actions.selectCurrentVideo(''));
      yield put(actions.clearVideos(''));
      throw new Error('No Videos Found!');
    }
    const videoURL = yield Storage.get(data.listLists.items[0].imageKey);

    yield put(actions.selectCurrentVideo(videoURL));
    yield put(actions.setOpen(false));
    yield put(actions.loadVideosSuccess(data.listLists.items));
    console.log('List created');
  } catch (e) {
    console.log('API Error', e);
  }
}

function* searchVideos(action: PayloadAction<any>) {
  const { searchInput, ascSort } = action.payload;
  if (searchInput === '') return yield loadVideos();
  try {
    const { data } = (yield API.graphql(
      graphqlOperation(searchLists, {
        filter: {
          title: {
            match: searchInput,
          },
        },
        sort: {
          field: 'createdAt',
          direction: ascSort ? 'asc' : 'desc',
        },
      }),
    )) as {
      data: SearchData;
      errors: any[];
    };
    console.log('data', data.searchLists.items);
    yield put(actions.searchVideosSuccess(data.searchLists.items));
    console.log('List created Search');
  } catch (e) {
    console.log('API Error Search', e);
    yield put(actions.loadVideosError(e));
  }
}

function* uploadVideo(action: PayloadAction<any>) {
  try {
    const { data } = (yield API.graphql(
      graphqlOperation(createList, { input: action.payload }),
    )) as {
      data: UploadData;
      errors: any[];
    };
    console.log('Saved data', data);
    yield put(actions.uploadVideosSuccess(data.createList));
  } catch (e) {
    console.log('API Upload Video Error', e);
    yield put(actions.loadVideosError(e));
  }
}
export function* homepageSaga() {
  yield takeLatest(actions.loadVideos.type, loadVideos);
  yield takeLatest(actions.searchVideos.type, searchVideos);
  yield takeLatest(actions.uploadVideo.type, uploadVideo);
}
