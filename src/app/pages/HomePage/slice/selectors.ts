import { createSelector } from '@reduxjs/toolkit';

import { RootState } from 'types';
import { initialState } from '.';

const selectSlice = (state: RootState) => state.homepage || initialState;

export const selectHomepage = createSelector([selectSlice], state => state);
export const selectVideos = createSelector(
  [selectSlice],
  state => state.videos,
);

export const selectCurrentVideo = createSelector(
  [selectSlice],
  state => state.currentVideo,
);

export const modalOpen = createSelector(
  [selectSlice],
  state => state.modalOpen,
);
