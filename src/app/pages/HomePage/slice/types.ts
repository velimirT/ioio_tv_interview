/* --- STATE --- */
export interface HomepageState {
  videos: Array<Item>;
  currentVideo: string;
  videosLoading: boolean;
  videosError: string;
  modalOpen: boolean;
  uploadVideoLoading: boolean;
  uploadVideoError: string;
}

interface Item {
  id: number;
  title: string;
  description: string;
  imageKey: string;
  createdAt: string;
}
