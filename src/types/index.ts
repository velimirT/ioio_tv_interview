import { RootState } from './RootState';

interface Item {
  id: number;
  title: String;
}

export type { RootState };
